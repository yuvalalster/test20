import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { GeneralsComponent } from './generals/generals.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { GeneralformComponent } from './generalform/generalform.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ClassifiedComponent } from './classified/classified.component';
import { DocformComponent } from './docform/docform.component';
import { HttpClientModule } from '@angular/common/http';
import { WelcomComponent } from './welcom/welcom.component';
import { ArticlesComponent } from './articles/articles.component';
import { MainsComponent } from './mains/mains.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import { DatePipe } from '@angular/common';
import {MatNativeDateModule} from '@angular/material';
import { SavedpostsComponent } from './savedposts/savedposts.component';




const appRoutes: Routes = [
    { path: 'generals', component: GeneralsComponent },
    { path: '',
      redirectTo: '/generals',
      pathMatch: 'full'
    },
    { path: 'generalform', component: GeneralformComponent},
    { path: 'welcom', component: WelcomComponent},
    { path: 'generalform/:id', component: GeneralformComponent},
    { path: 'signup', component: SignUpComponent},
    { path: 'login', component: LoginComponent},
    { path: 'classify', component: DocformComponent},
    { path: 'classified', component: ClassifiedComponent},
    { path: 'articles', component: ArticlesComponent},
    { path: 'mains', component: MainsComponent},
    { path: 'savedposts', component: SavedpostsComponent},
    { path: 'articles', component: ArticlesComponent},
  ];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    GeneralsComponent,
    GeneralformComponent,
    LoginComponent,
    SignUpComponent,
    ClassifiedComponent,
    DocformComponent,
    WelcomComponent,
    ArticlesComponent,
    MainsComponent,
    SavedpostsComponent,
  ],
  imports: [
    MatExpansionModule,
    MatCardModule,
    RouterModule.forRoot(
            appRoutes,
            { enableTracing: true } // <-- debugging purposes only
          ),  
    FormsModule,
    MatFormFieldModule,
    MatInputModule,

    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'test20-5fa0d'),
    HttpClientModule,
    MatDatepickerModule,
    MatSelectModule,
    MatNativeDateModule,
    
  ],
  providers: [AngularFireAuth, AngularFirestore, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }