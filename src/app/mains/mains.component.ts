import { Component, OnInit } from '@angular/core';
import { Main } from '../interfaces/main';
import { MainService } from '../main.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-mains',
  templateUrl: './mains.component.html',
  styleUrls: ['./mains.component.css']
})
export class MainsComponent implements OnInit {

  //geting posts from json not from db
  mains$:Main[];
  comments$: Comment[];
  userId:string;
  text:string;
  postid:number;
  
   
  

  

  constructor(private mainService:MainService,public authService:AuthService) { }

  postFunc(title:String,body:String){
   
    this.mainService.addPost(title,body);
    
 }

  ngOnInit() {

     
    //geting posts from json
    this.mainService.getMains().subscribe(data => this.mains$ = data)
    this.mainService.getComment()
    .subscribe(data =>this.comments$ = data );

  }

}
