import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Main } from './interfaces/main';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class MainService {
 
  apiUrl='https://jsonplaceholder.typicode.com/posts';
  commentapi = "http://jsonplaceholder.typicode.com/comments";
  mainCollection: AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  constructor(private http: HttpClient, private db: AngularFirestore,private authService:AuthService) { }

  addPost(title:String,body:String){
    const post = {title:title, body:body}
    this.db.collection('posts').add(post)  
  } 

  
  getPosts(userId): Observable<any[]> {
    //const ref = this.db.collection('generals');
    //return ref.valueChanges({idField: 'id'});
    this.mainCollection = this.db.collection(`users/${userId}/posts`);
    console.log('posts collection created');
    return this.mainCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data;
      }))
    );    
  } 


  getPost(userId, id:string):Observable<any>{
    return this.db.doc(`users/${userId}/posts/${id}`).get();
  }



  getMains(){
    return this.http.get<Main[]>(this.apiUrl)
  }

  getComment(){
    return this.http.get<Comment[]>(this.commentapi);
  }
}
