import { Injectable } from '@angular/core';
import { ClassifyService } from './classify.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ClassifiedService {

  constructor(private db: AngularFirestore) { }

  getArticles():Observable<any[]>{
    return this.db.collection('articles').valueChanges({idField:'id'});
     }
  

  addArticle(category:String,body:String,image:String){
    const article = {category:category, body:body,image:image}
    this.db.collection('articles').add(article)  
  }  

  deleteArticle(id:string){
    this.db.doc(`articles/${id}`).delete();
  }

}
