import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneralsService } from './../generals.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-generalform',
  templateUrl: './generalform.component.html',
  styleUrls: ['./generalform.component.css']
})
export class GeneralformComponent implements OnInit {

  constructor(private generalsService:GeneralsService,
              private authService:AuthService, 
              private router:Router,
              private route: ActivatedRoute) { }

  title:string;
  //author:string; 
  date:Date;
  rooms:object[] = [{id:1, name:'confrence room A'},{id:2, name:'confrence room B'},
    {id:3, name:'confrence room C'}, {id:4, name:'confrence room D'}];
  room:string;
  id:string;
  userId:string;
  isEdit:boolean = false;
  buttonText:string = 'Add general' 
  
  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.generalsService.updateGeneral(this.userId, this.id,this.title,this.date,this.room);
    } else {
      console.log('In onSubmit');
      this.generalsService.addGeneral(this.userId,this.title,this.date,this.room)
    }
    this.router.navigate(['/generals']);  
  }  

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
        if(this.id) {
          this.isEdit = true;
          this.buttonText = 'Update general'
          this.generalsService.getGeneral(this.userId,this.id).subscribe(
            general => {
            console.log(general.data().date)
            console.log(general.data().title)
            console.log(general.data().room)
            this.title = general.data().title;
            this.room = general.data().room;
            this.date = general.data().date;})        
      }
   })
  }
}
