import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-savedposts',
  templateUrl: './savedposts.component.html',
  styleUrls: ['./savedposts.component.css']
})
export class SavedpostsComponent implements OnInit {

  panelOpenState = false;
  posts$:Observable<any[]>;
  userId:string;
  //id:string;

  constructor(public authService:AuthService,public mainService:MainService) { }

  ngOnInit() {

    // console.log("NgOnInit started")  
    // this.authService.getUser().subscribe(
    //   user => {
    //     this.userId = user.uid;
    //     this.posts$ = this.mainService.getPosts(this.userId);

  }
  
}
