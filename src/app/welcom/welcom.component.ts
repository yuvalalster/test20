import { Users } from './../interfaces/users';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-welcom',
  templateUrl: './welcom.component.html',
  styleUrls: ['./welcom.component.css']
})
export class WelcomComponent implements OnInit {
  auth: any;
  //auth: any;

  

  constructor(public authService:AuthService) { }

  userId:string;
  users$:Observable<any>;

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.email;
      }
    )
  }

}

