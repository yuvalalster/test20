import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { GeneralsService } from './../generals.service';
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-generals',
  templateUrl: './generals.component.html',
  styleUrls: ['./generals.component.css']
})
export class GeneralsComponent implements OnInit {

  panelOpenState = false;
  //books: object[] =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
  //books: object[];
  generals$:Observable<any[]>;
  userId:string;

  deleteGeneral(id){
    this.generalservice.deleteGeneral(this.userId,id)
    console.log(id);
  }
  
  constructor(private generalservice:GeneralsService,
              public authService:AuthService, public datePipe:DatePipe) { }

  ngOnInit() { 
    console.log("NgOnInit started")  
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.generals$ = this.generalservice.getGenerals(this.userId); 
      }
    )
  }
}
