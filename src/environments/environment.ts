// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebaseConfig: {
    apiKey: "AIzaSyB9vOe6TBec7FihJn96ROYz5oUs7u6jzwY",
    authDomain: "test20-edfc1.firebaseapp.com",
    databaseURL: "https://test20-edfc1.firebaseio.com",
    projectId: "test20-edfc1",
    storageBucket: "test20-edfc1.appspot.com",
    messagingSenderId: "1080972314301",
    appId: "1:1080972314301:web:c23137051874109c2ad9d4",
    
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
